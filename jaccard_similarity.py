from tfidf import  Tfidf
import string
from nltk.stem import WordNetLemmatizer
from nltk.corpus import stopwords
import numpy as np
from keras import backend as K

tf = Tfidf()
wordnet_lemmatizer = WordNetLemmatizer()

class Jaccard_similarity:


    def jaccard_sim(self,text,b):
            label = []
            for items in b:
                #         print(items)

                inter = len(items.intersection(text))
                union = len(items.union(text))
                jacca = float(inter / union) * 100

                label.append(jacca)

            return np.argmax(label)

            # This function lemmatize the given strings and remove the puncutation and remove the stopwords

    def preprocess(self, items):
        li = []

        lemmas = [wordnet_lemmatizer.lemmatize(items, pos='v')]
        text = ''.join(lemmas)

        nopunc = [char for char in text if char not in string.punctuation]
        nopunc = ''.join(nopunc)

        word = [word for word in nopunc.split() if word.lower() not in stopwords.words('english')]
        word = ' '.join(word)

        return word



    def jaccar(self,inputt,data):
        # Jaccard function which gives the jaccard similarity in percentage and appended in a label and maximum
        # percentage index is returned




        # calling preprocess function to get the processed sentence list



        nopun = data['sentence'].apply(self.preprocess)
        # nopun = self.preprocess(data['sentence'])

        # since jaccard similarity only take set as input and every word should be separated so splitting
        # the data and making set of it
        b = []
        for filename in nopun:
            #     print(type(filename))
            a = filename.split()
            b.append(set(a))


        # Taking input from user and and splitting it and making set of it as this format is expexted
        # by the jaccard function
        # inputt = input('Enter the text')
        sep = inputt.split()
        sep = set(sep)
        # tex = {'Howdy','good','things','bye'}
        c = self.jaccard_sim(sep,b)
        K.clear_session()
        # label = list(data['intent'])
        # print('The intent of this sentence using jaccard similarity is ::', label[c])
        return c
