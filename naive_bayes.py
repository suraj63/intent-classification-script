from sklearn.naive_bayes import MultinomialNB
from tfidf import Tfidf
from sklearn.metrics import classification_report
from sklearn.model_selection import train_test_split
tf = Tfidf()
class Naive_bayes:


    def naive(self,data,mode):
        X_train, X_test, y_train, y_test = train_test_split(data['sentence'], data['intent'], test_size=0.33,
                                                            random_state=42)
        tran = tf.transform(X_train,mode)

        # using naive bayes classifer to find the probablity of each text and identify its intent
        model = MultinomialNB(alpha=0.1)
        mod = model.fit(tran, y_train)
        predictions = model.predict(tf.transform(X_test,mode))
        print (classification_report(y_test, predictions))
        res = "Run sucessfully"
        return res