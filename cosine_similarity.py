from tfidf import  Tfidf
tf = Tfidf()
from sklearn.metrics.pairwise import cosine_similarity
import numpy as np

class Cosine_similarity:

    def check(self,transform,text):
        vect = []
        for items in transform:
            result = cosine_similarity(text,items)
            vect.append(result)
        ind = np.argmax(vect)
        return ind

    def cos(self,data,model,text):
        # Using cosine similarity to identify intent from a  given text
        transform = tf.transform(data['sentence'],model)



        label = list(data['intent'])


        # test2 = [input('Enter text')]
        te1 = tf.transform(text,model)
        index = self.check(transform,te1)
        # print("The intent is::", label[index])


        return (label,index)