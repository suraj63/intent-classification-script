from flask import Flask,request,jsonify,render_template
import numpy as np
import pandas as pd
# import matplotlib.pyplot as plt
from sklearn.feature_extraction.text import TfidfVectorizer
import nltk
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics import roc_auc_score, roc_curve
from sklearn.metrics.pairwise import cosine_similarity
from tfidf import Tfidf
from naive_bayes import Naive_bayes
from cosine_similarity import Cosine_similarity
from jaccard_similarity import Jaccard_similarity

tf = Tfidf()
nb = Naive_bayes()
cosine = Cosine_similarity()
jaccardsi= Jaccard_similarity()

tft = TfidfVectorizer()


app = Flask(__name__)


@app.route('/',methods=['GET','POST'])
def main():
    return render_template('index.html')

@app.route('/analyze',methods=['GET','POST'])
def analyze():
    text = [request.form['intent']]
    data = pd.read_csv('intent_dataset.csv')
    tft = TfidfVectorizer()
    model = tft.fit(data['sentence'])



    # vect = transform(data['sentence'], model)

    # vectorized =  tf.vectorize(data)
    naiv = nb.naive(data,model)

    lab,ind = cosine.cos(data,model,text)
    cosi = lab[ind]

    jacndex = jaccardsi.jaccar(request.form['intent'],data)
    jac = lab[jacndex]

    res = "Cosine Similarity::",cosi +"  and  Jaccard Similarity::",jac

    return render_template('index.html', result=res)




if __name__ == '__main__':
    app.run(debug=True)


